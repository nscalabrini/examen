﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using BE;

namespace DAL
{
    public class MP_RECIBO
    {
        private Acceso acceso = new Acceso();
        BE.Persona persona = new Persona();

        public void Insertar(BE.Recibo recibo)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@id_persona",persona.Id ));
            parameters.Add(acceso.CrearParametro("@fec", recibo.Fecha));
            parameters.Add(acceso.CrearParametro("@sbruto", recibo.Sbruto));
            parameters.Add(acceso.CrearParametro("@sneto", recibo.Sneto));
            acceso.Escribir("Recibo_INSERTAR", parameters);

            acceso.Cerrar();
        }

        public List<BE.Recibo> Listartodo(BE.Recibo recibo)
        {
            List<BE.Recibo> lista = new List<BE.Recibo>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@id_persona", persona.Id));
            parameters.Add(acceso.CrearParametro("@id_recibo", recibo.Id_recibo));

            DataTable tabla = acceso.Leer("LISTAR_TODO", parameters);
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Recibo r = new BE.Recibo();
                r.Id_recibo = int.Parse(registro[0].ToString());
                r.Id_persona = int.Parse(registro[1].ToString());
                r.Fecha = DateTime.Parse(registro[2].ToString());
                r.Sbruto = float.Parse(registro[3].ToString());
                r.Sneto = float.Parse(registro[4].ToString());
                                
                lista.Add(r);
            }
            return lista;
        }

        public List<BE.Recibo> Listarindiv(BE.Recibo recibo)
        {
            List<BE.Recibo> lista = new List<BE.Recibo>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@id_persona", persona.Id));
            parameters.Add(acceso.CrearParametro("@id_persona", recibo.Id_recibo));

            DataTable tabla = acceso.Leer("LISTAR_INDIVIDUAL", parameters);
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Recibo r = new BE.Recibo();
                r.Id_recibo = int.Parse(registro[0].ToString());
                r.Id_persona = int.Parse(registro[1].ToString());
                r.Fecha = DateTime.Parse(registro[2].ToString());
                r.Sbruto = float.Parse(registro[3].ToString());
                r.Sneto = float.Parse(registro[4].ToString());

                lista.Add(r);
            }
            return lista;
        }

    }
}
