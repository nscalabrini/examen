﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using BE;

namespace DAL
{
   public class MP_Concepto
    {
        private Acceso acceso = new Acceso();
        BE.Persona persona = new Persona();
        BE.Recibo recibo = new Recibo();

        public void Insertar(BE.Concepto concepto)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@id_persona", persona.Id));
            parameters.Add(acceso.CrearParametro("@id_recibo", recibo.Fecha));
            parameters.Add(acceso.CrearParametro("@detalle", concepto.Detalle));
            parameters.Add(acceso.CrearParametro("@porc", concepto.Detalle));
            acceso.Escribir("CONCEPTO_INSERTAR", parameters);

            acceso.Cerrar();
        }

        public List<BE.Concepto> Listarconcepto (BE.Concepto concepto)
        {
            List<BE.Concepto> lista = new List<BE.Concepto>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@id_Concepto", concepto.Id_Concepto));
            parameters.Add(acceso.CrearParametro("@id_recibo", recibo.Id_recibo)); 
            parameters.Add(acceso.CrearParametro("@id_recibo", recibo.Id_recibo));
            parameters.Add(acceso.CrearParametro("@detalle", concepto.Detalle));
            parameters.Add(acceso.CrearParametro("@porc", concepto.Porc));

            DataTable tabla = acceso.Leer("CONCEPTO_LISTAR", parameters);
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Concepto c = new BE.Concepto();
                c.Id_Concepto= int.Parse(registro[0].ToString());
                c.Id_Concepto = int.Parse(registro[1].ToString());
                c.Id_Persona = int.Parse(registro[2].ToString());
                c.Detalle = (registro[3].ToString());
                c.Porc = float.Parse(registro[4].ToString());
               
                lista.Add(c);
            }
            return lista;
        }

     

    }
}
