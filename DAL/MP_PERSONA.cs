﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
   public  class MP_PERSONA
    {

        private Acceso acceso = new Acceso();

        public void Insertar( BE.Persona persona )
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@nom", persona.Nombre));
            parameters.Add(acceso.CrearParametro("@leg", persona.Legajo));
            parameters.Add(acceso.CrearParametro("@cuil", persona.Cuil));
            parameters.Add(acceso.CrearParametro("@fec", persona.Fecha));
            acceso.Escribir("PERSONA_INSERTAR", parameters);

            acceso.Cerrar();
        }

        public void Editar(BE.Persona persona )
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@nom", persona.Nombre));
            parameters.Add(acceso.CrearParametro("@leg", persona.Legajo));
            parameters.Add(acceso.CrearParametro("@cuil", persona.Cuil));
            parameters.Add(acceso.CrearParametro("@fec", persona.Fecha));
            parameters.Add(acceso.CrearParametro("@id", persona.Id));
            acceso.Escribir("PERSONA_EDITAR", parameters);

            acceso.Cerrar();
        }

        public void Borrar(BE.Persona persona)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@id", persona.Id));
            acceso.Escribir("PERSONA_BORRAR", parameters);

            acceso.Cerrar();
        }

        public  List<BE.Persona> Listar()
        {
            List<BE.Persona> lista = new List<BE.Persona>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("PERSONA_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Persona p = new BE.Persona();
                p.Nombre = registro[1].ToString();
                p.Id = int.Parse(registro[0].ToString());
                p.Legajo = int.Parse(registro[2].ToString());
                p.Cuil = int.Parse(registro[3].ToString());
                p.Fecha = DateTime.Parse(registro[4].ToString());
                lista.Add(p);
            }
            return lista;
        }




    }
}
