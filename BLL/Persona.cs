﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Persona
    {
        DAL.MP_PERSONA mp = new DAL.MP_PERSONA();

        public void Grabar(BE.Persona persona)
        {

            if (persona.Id == 0)
            {
                mp.Insertar(persona);

            }
            else
            {
                mp.Editar(persona);
            }

        }

        public void Borrar(BE.Persona persona)
        {
            mp.Borrar(persona);
        }

        public List<BE.Persona> Listar()
        {
          return  mp.Listar();

        }



    }
}
