﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Recibo : Form
    {
        BLL.Recibo gestor = new BLL.Recibo();
        BLL.Persona gestorp = new BLL.Persona();
        public Recibo()
        {
            InitializeComponent();
        }

        private void Recibo_Load(object sender, EventArgs e)
        {
            //Enlazar();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            BE.Recibo r = new BE.Recibo();
            r.Fecha = dateTimePicker1.Value;
            r.Sbruto= float.Parse(txtsbruto.Text);
            r.Sneto= float.Parse(txtsneto.Text);
            
            gestor.Grabar(r);
        }
        private void Enlazar()
        {
            BE.Recibo r = new BE.Recibo();
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestor.LiListarindiv(r);

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtsneto.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            txtsneto.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BE.Recibo r = new BE.Recibo();
            r.Fecha = dateTimePicker1.Value;
            r.Sbruto = float.Parse(txtsbruto.Text);
            r.Sneto = float.Parse(txtsneto.Text);

            gestor.LiListarindiv(r);

            Enlazar();


        }
    }
}
