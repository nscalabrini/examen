﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
        }

        private void empleadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Empleado frmemp = new Empleado();
            frmemp.Show();
        }

        private void conceptoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Concepto frmconcepto = new Concepto();
            frmconcepto.Show();
        }

        private void reciboToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Recibo frmrecibo = new Recibo();
            frmrecibo.Show();

        }

        private void Principal_Load(object sender, EventArgs e)
        {

        }
    }
}
