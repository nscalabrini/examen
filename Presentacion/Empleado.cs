﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Empleado : Form
    {

        BLL.Persona gestor = new BLL.Persona();

        public Empleado()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestor.Listar();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            BE.Persona p = new BE.Persona();
            p.Nombre = txtnombre.Text;
            p.Legajo = int.Parse( txtlegajo.Text);
            p.Cuil = int.Parse(txtcuil.Text);
            p.Fecha = dateTimePicker1.Value;
            gestor.Grabar(p);

            Enlazar();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtId.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            txtnombre.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtlegajo.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtcuil.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            
        }



        private void button2_Click(object sender, EventArgs e)
        {
            BE.Persona p = new BE.Persona();

            p.Id = int.Parse(txtId.Text);
            p.Nombre = txtnombre.Text;
            p.Legajo = int.Parse(txtlegajo.Text);
            p.Cuil = int.Parse(txtcuil.Text);
            p.Fecha = dateTimePicker1.Value;

            gestor.Grabar(p);
            Enlazar();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BE.Persona p = new BE.Persona();

            p.Id = int.Parse(txtId.Text);

            gestor.Borrar(p);
            Enlazar();
        }

     }
}
