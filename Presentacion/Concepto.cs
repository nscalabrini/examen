﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Concepto : Form
    {
        BLL.Concepto gestor = new BLL.Concepto();
        public Concepto()
        {
            InitializeComponent();
        }

        private void Concepto_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            BE.Concepto c = new BE.Concepto();
            c.Detalle = txtdetalle.Text;
            c.Porc = float.Parse(txtporcentaje.Text);
            gestor.Grabar(c);
        }
    }
}
