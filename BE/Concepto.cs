﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Concepto
    {
        private int id_concepto;

        public int Id_Concepto
        {
            get { return id_concepto; }
            set { id_concepto = value; }
        }

        private int id_persona;

        public int Id_Persona
        {
            get { return id_persona; }
            set { id_persona = value; }
        }

        private int id_recibo;

        public int Id_recibo
        {
            get { return id_recibo; }
            set { id_recibo = value; }
        }

        private String detalle;

        public String Detalle
        {
            get { return detalle; }
            set { detalle = value; }
        }

        private float porc;

        public float Porc
        {
            get { return porc; }
            set { porc = value; }
        }

    }

}
