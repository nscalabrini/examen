﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
   public class Recibo
    {
        private DateTime fecha;

        public DateTime Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }

        private float sbruto;

        public float Sbruto
        {
            get { return sbruto; }
            set { sbruto = value; }
        }

        private float sneto;

        public float Sneto
        {
            get { return sneto; }
            set { sneto = value; }
        }
        private int id_recibo;

        public int Id_recibo
        {
            get { return id_recibo; }
            set { id_recibo = value; }
        }

        private int id_persona;

        public int Id_persona
        {
            get { return id_persona; }
            set { id_persona = value; }
        }


    }
}
