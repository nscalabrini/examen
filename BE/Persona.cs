﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Persona
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private int legajo;

        public int Legajo
        {
            get { return legajo; }
            set { legajo = value; }
        }
        private int cuil;

        public int Cuil
        {
            get { return cuil; }
            set { cuil = value; }
        }
        private DateTime dateTime;

        public DateTime Fecha
        {
            get { return dateTime; }
            set { dateTime = value; }
        }


    }
}
